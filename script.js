

const gameContainer = document.getElementById("game");
const game = document.querySelector(".game");

// const COLORS = [
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",

// ];

const COLORS = [
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
]


const level2Colors = [
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
	"url(gifs/6.gif)",
	"url(gifs/7.gif)",
	"url(gifs/8.gif)",
	"url(gifs/9.gif)",
	"url(gifs/10.gif)",
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
	"url(gifs/6.gif)",
	"url(gifs/7.gif)",
	"url(gifs/8.gif)",
	"url(gifs/9.gif)",
	"url(gifs/10.gif)",
]

const level3Colors = [
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
	"url(gifs/6.gif)",
	"url(gifs/7.gif)",
	"url(gifs/8.gif)",
	"url(gifs/9.gif)",
	"url(gifs/10.gif)",
	"url(gifs/11.gif)",
	"url(gifs/12.gif)",
	"url(gifs/13.gif)",
	"url(gifs/14.gif)",
	"url(gifs/15.gif)",
	"url(gifs/1.gif)",
	"url(gifs/2.gif)",
	"url(gifs/3.gif)",
	"url(gifs/4.gif)",
	"url(gifs/5.gif)",
	"url(gifs/6.gif)",
	"url(gifs/7.gif)",
	"url(gifs/8.gif)",
	"url(gifs/9.gif)",
	"url(gifs/10.gif)",
	"url(gifs/11.gif)",
	"url(gifs/12.gif)",
	"url(gifs/13.gif)",
	"url(gifs/14.gif)",
	"url(gifs/15.gif)",
]
// const level2Colors = [
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",
// 	"hotPink",
// 	"hotPink",
// 	"brown",
// 	"brown",
// 	"mediumseagreen",
// 	"mediumseagreen",
// 	"grey",
// 	"grey",
// 	"springgreen",
// 	"springgreen",

// ];

// const level3Colors = [
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",
// 	"red",
// 	"blue",
// 	"green",
// 	"orange",
// 	"purple",
// 	"hotPink",
// 	"hotPink",
// 	"brown",
// 	"brown",
// 	"mediumseagreen",
// 	"mediumseagreen",
// 	"grey",
// 	"grey",
// 	"springgreen",
// 	"springgreen",
// 	"white",
// 	"white",
// 	"midnightblue",
// 	"midnightblue",
// 	"salmon",
// 	"salmon",
// 	"gold",
// 	"gold",
// 	"mediumvioletred",
// 	"mediumvioletred"
// ];
const levels = document.querySelector(".levels");
const startButton = document.querySelector(".start-btn");
const secondLevel = document.querySelector(".level2");
const thirdLevel = document.querySelector(".level3");
let divCount = 0;
let checkArray = [];
let clickCount = 0;
let score = 0;
let eventArray = [];
const chooseLevel = document.querySelector("h2");
const reset = document.querySelector(".reset-btn");
const innerBox = document.querySelector(".innerBox div");
const best = document.querySelector(".bestCount");
const popUp = document.querySelector(".popUp");
const confirmReset = document.querySelector(".confirmReset");
const cancel = document.querySelector(".confirmCancel");

if(localStorage.getItem("bestScore") != null) {
	best.innerHTML = localStorage.getItem("bestScore");
} else {
	best.innerHTML = "0";
}

function shuffle(array) {
	let counter = array.length;

	while (counter > 0) {
		let index = Math.floor(Math.random() * counter);

		counter--;

		let temp = array[counter];
		array[counter] = array[index];
		array[index] = temp;
	}

	return array;
}

let shuffledColors = shuffle(COLORS);

function createDivsForColors(colorArray) {
	let idNumber = 1;
	// const middle = document.createElement("div");
	// middle.classList.add("middle");
	const upperDiv = document.createElement("div");
	upperDiv.classList.add("innerBox");
	const lowerDiv = document.createElement("div");
	lowerDiv.classList.add("innerBox");

	gameContainer.append(upperDiv, lowerDiv);
	divCount = colorArray.length;

	let limit = colorArray.length / 2;
	for (let color of colorArray) {

		const newDiv = document.createElement("div");


		// give it a class attribute for the value we are looping over
		newDiv.classList.add(color);
		newDiv.id = idNumber;
		if (idNumber > limit) {
			lowerDiv.append(newDiv);
		} else {
			upperDiv.append(newDiv);
		}

		idNumber += 1;
		newDiv.addEventListener("click", handleCardClick);

		// gameContainer.append(newDiv);
	}
	if (divCount > 20) {
		let levelThree = document.querySelectorAll(".innerBox div");
		console.log(levelThree);
		levelThree.forEach(box => box.style.width = "90px")
	}
}



function handleCardClick(event) {
	// you can use event.target to see which element was clicked

	let color = event.target.className;
	if (!eventArray.includes(event.target)) {
		eventArray.push(event.target);

	}

	if (eventArray.length < 3) {
		event.target.style.transition = "0.5s";
		event.target.classList.toggle("rotate");
		event.target.style.backgroundImage = color;
		event.target.style.backgroundColor = "none";
		clickCount += 1;
		document.querySelector(".moveCount").innerHTML = " : " + clickCount
	}

	if (eventArray.length === 2) {
		if (eventArray[0].className === eventArray[1].className) {
			checkArray.push(eventArray[0]);
			checkArray.push(eventArray[1])
			eventArray[0].removeEventListener("click", handleCardClick);
			eventArray[1].removeEventListener("click", handleCardClick);
			score += 10;
			document.querySelector(".scoreCount").innerHTML = ": " + score;
			eventArray = [];

		} else {
			setTimeout(() => {

				eventArray[0].style.backgroundImage = "url(gifs/box-bg-img.jpg)"
				eventArray[0].style.transition = "0.5s";

				eventArray[0].classList.toggle("rotate");

				eventArray[1].style.backgroundImage = "url(gifs/box-bg-img.jpg)"
				eventArray[1].style.transition = "0.6s";
				eventArray[1].classList.toggle("rotate");
				eventArray = [];
			}, 800);
		}
	}
	console.log(divCount);
	if (checkArray.length === divCount) {
		document.querySelector(".moveCount").innerHTML = "";
		document.querySelector(".scoreCount").innerHTML = "";
		levels.style.display = "block";
		document.querySelector(".game").style.display = "block";
		document.querySelector(".game").style.left = "-3000px";
		document.querySelector(".finalScore").style.display = "block";
		document.querySelector(".finalScore").style.right = "0";
		let boxes = document.querySelectorAll(".innerBox");
		console.log(boxes);
		boxes[0].remove();
		boxes[1].remove();
		startButton.style.width = "150px";
		startButton.style.height = "50px";
		startButton.style.fontSize = "15px";
		chooseLevel.style.fontSize = "30px"
		secondLevel.style.width = "150px";
		secondLevel.style.height = "50px";
		secondLevel.style.fontSize = "15px";
		thirdLevel.style.width = "150px";
		thirdLevel.style.height = "50px";
		thirdLevel.style.fontSize = "15px";
		console.log(clickCount);
		checkArray = [];
		document.querySelector(".scores").innerHTML = "Total score based on moves is : " + Math.floor(score / clickCount) + 10;
		if (clickCount < localStorage.getItem("bestScore") || localStorage.getItem("bestScore") == null) {
			document.querySelector(".moves").innerHTML = "Your new best moves : " + clickCount;
			localStorage.setItem("bestScore", clickCount);
		} else {
			document.querySelector(".moves").innerHTML = "Your current score is : " + clickCount + "<br>Your best moves count is : " + localStorage.getItem("bestScore");

		}
		score = 0;
	}

}


startButton.addEventListener("click", () => {
	document.querySelector(".moveCount").innerHTML = "";
	clickCount = 0;
	document.querySelector(".scoreCount").innerHTML = "";
	score = 0;
	document.querySelector(".game").style.display = "block";
	document.querySelector(".finalScore").style.right = "-3000px";
	document.querySelector(".finalScore").style.display = "none";
	createDivsForColors(shuffle(COLORS));
	startButton.style.width = "0";
	startButton.style.height = "0";
	startButton.style.fontSize = "0";
	chooseLevel.style.fontSize = "0"
	secondLevel.style.width = "0";
	secondLevel.style.height = "0";
	secondLevel.style.fontSize = "0";
	thirdLevel.style.width = "0";
	thirdLevel.style.height = "0";
	thirdLevel.style.fontSize = "0";
	setTimeout(() => {
		levels.style.display = "none";
	}, 300)
	reset.setAttribute("id", "COLORS");
	setTimeout(() => {
		game.style.display = "block";
		game.style.left = "0";
	}, 300);
})

secondLevel.addEventListener("click", () => {
	document.querySelector(".moveCount").innerHTML = "";
	clickCount = 0;
	document.querySelector(".scoreCount").innerHTML = "";
	score = 0;
	document.querySelector(".game").style.display = "block";
	document.querySelector(".finalScore").style.right = "-3000px";
	document.querySelector(".finalScore").style.display = "none";
	createDivsForColors(shuffle(level2Colors));
	chooseLevel.style.fontSize = "0"
	startButton.style.width = "0";
	startButton.style.height = "0";
	startButton.style.fontSize = "0";
	secondLevel.style.width = "0";
	secondLevel.style.height = "0";
	secondLevel.style.fontSize = "0";
	thirdLevel.style.width = "0";
	thirdLevel.style.height = "0";
	thirdLevel.style.fontSize = "0";
	reset.setAttribute("id", "level2Colors");
	// innerBox.style.backgroundSize = ""
	setTimeout(() => {
		game.style.display = "block";
		game.style.left = "0";
	}, 300);
})

thirdLevel.addEventListener("click", () => {
	document.querySelector(".moveCount").innerHTML = "";
	clickCount = 0;
	document.querySelector(".scoreCount").innerHTML = "";
	score = 0;
	document.querySelector(".game").style.display = "block";
	document.querySelector(".finalScore").style.right = "-3000px";
	document.querySelector(".finalScore").style.display = "none";
	// innerBox.style.width = "100px";
	// innerBox.style.height = "200px";
	createDivsForColors(shuffle(level3Colors));
	chooseLevel.style.fontSize = "0"
	startButton.style.width = "0";
	startButton.style.height = "0";
	startButton.style.fontSize = "0";
	secondLevel.style.width = "0";
	secondLevel.style.height = "0";
	secondLevel.style.fontSize = "0";
	thirdLevel.style.width = "0";
	thirdLevel.style.height = "0";
	thirdLevel.style.fontSize = "0";
	reset.setAttribute("id", "level3Colors");
	setTimeout(() => {
		game.style.display = "block";
		document.querySelector("body").margin = "100px"
		game.style.left = "0";
	}, 300);
})

reset.addEventListener("click", () => {
	popUp.style.display = "block";
	popUp.style.width = "350px";
	// document.querySelector(".moveCount").innerHTML = "";
	// clickCount = 0;
	// document.querySelector(".scoreCount").innerHTML = "";
	// score = 0;
	// let mixed;
	// if (reset.id === "COLORS") {
	// 	mixed = shuffle(COLORS);
	// } else if (reset.id === "level2Colors") {
	// 	mixed = shuffle(level2Colors);
	// } else {
	// 	mixed = shuffle(level3Colors);
	// }
	// console.log(reset.id)
	// let boxes = document.querySelectorAll(".innerBox");
	// console.log(boxes);
	// boxes[0].remove();
	// boxes[1].remove();
	// createDivsForColors(mixed);
})

cancel.addEventListener("click", () => {
	popUp.style.display = "none";
})

confirmReset.addEventListener("click", () => {
	popUp.style.display = "none";
	document.querySelector(".moveCount").innerHTML = "";
	clickCount = 0;
	document.querySelector(".scoreCount").innerHTML = "";
	score = 0;
	let mixed;
	if (reset.id === "COLORS") {
		mixed = shuffle(COLORS);
	} else if (reset.id === "level2Colors") {
		mixed = shuffle(level2Colors);
	} else {
		mixed = shuffle(level3Colors);
	}
	console.log(reset.id)
	let boxes = document.querySelectorAll(".innerBox");
	console.log(boxes);
	boxes[0].remove();
	boxes[1].remove();
	createDivsForColors(mixed);
})
